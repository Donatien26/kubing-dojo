package fr.insee.coding.dojo.back.beans;

public class Config {

    private String title;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
