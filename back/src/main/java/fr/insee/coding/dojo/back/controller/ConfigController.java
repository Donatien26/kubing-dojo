package fr.insee.coding.dojo.back.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.insee.coding.dojo.back.beans.Config;

@RestController
@RequestMapping(value = { "/config" })
public class ConfigController {

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public Config getMethodName() {
        Config config = new Config();
        config.setTitle("Appli déployée avec kubernetes");
        return config;
    }

}
