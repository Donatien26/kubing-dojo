#!/bin/sh
cat <<EOF > /usr/share/nginx/html/configuration.json
{
	"apiURL": "${API_URL}"
}
EOF

exec "$@"