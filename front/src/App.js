import logo from "./logo.svg";
import "./App.css";
import React, { useEffect, useState } from "react";
import * as API from "./api/api-call";

const App = () => {
  const [config, setConfig] = useState(null);
  useEffect(() => {
    API.getAppConfig().then((res) => setConfig(res));
  }, []);
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>{config?.title}</p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
};

export default App;
