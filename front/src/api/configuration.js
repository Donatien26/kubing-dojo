const configFilePath = `${window.location.origin}/configuration.json`;

const getConfigFile = () =>
  fetch(configFilePath, {
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  }).then((res) => res.json());

let apiBaseURL = "";

const getConfig = (property) => getConfigFile().then((json) => json[property]);

const getApiBaseURL = () => {
  if (!apiBaseURL) apiBaseURL = getConfig("apiURL");
  return apiBaseURL || getConfig("apiURL");
};

export const configWrapper = (service) =>
  getApiBaseURL().then((BASE_URL) => service(BASE_URL));
