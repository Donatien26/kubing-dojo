import { configWrapper } from "./configuration";

export const getAppConfig = () =>
  configWrapper((baseURL) =>
    fetch(`${baseURL}/config/`).then((res) => res.json())
  );
